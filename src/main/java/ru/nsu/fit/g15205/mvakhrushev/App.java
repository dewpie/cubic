package ru.nsu.fit.g15205.mvakhrushev;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class App
{
    public static void main( String[] args )
    {
        if (args.length < 5) {
            System.out.println("Недостаточно аргументов");
            return ;
        }

        double e, delta, a, b, c;

        e = Double.valueOf(args[0]);
        delta = Double.valueOf(args[1]);

        a = Double.valueOf(args[2]);
        b = Double.valueOf(args[3]);
        c = Double.valueOf(args[4]);

        Cubic exp = new Cubic(a, b, c, delta, e);
        ArrayList<Double> roots = exp.getRoots();

        // Выводим с точностью до 6 знаков после запятой
        for (Double root : roots) {
            System.out.println(new DecimalFormat("#####0.00000").format(root));
        }
    }
}