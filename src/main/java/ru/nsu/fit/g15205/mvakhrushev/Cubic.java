package ru.nsu.fit.g15205.mvakhrushev;

import java.util.ArrayList;

public class Cubic {
    private double a = 0;
    private double b = 0;
    private double c = 0;

    private double delta = 0;
    private double e = 0;

    private double alpha = 0;
    private double beta = 0;

    private boolean solved = false;

    private ArrayList<Double> roots = new ArrayList<>();
    private ArrayList<Interval> local = new ArrayList<>();
    private ArrayList<Interval> global = new ArrayList<>();

    Cubic(double a, double b, double c, double delta, double e) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.delta = delta;
        this.e = e;

        this.alpha = Math.min(getRoot(3, 2 * a, b, 1), getRoot(3, 2 * a, b, -1));
        this.beta = Math.max(getRoot(3, 2 * a, b, 1), getRoot(3, 2 * a, b, -1));
    }

    public ArrayList<Double> getRoots() {
        if (!solved) {
            findIntervals();
            localizeIntervals();
            findRoots();

            solved = true;
        }

        return roots;
    }

    public double a() {
        return this.a;
    }

    public void a(double _a) {
        this.a = _a;
    }

    public double b() {
        return this.b;
    }

    public void b(double _b) {
        this.b = _b;
    }

    public double c() {
        return this.c;
    }

    public void c(double _c) {
        this.c = _c;
    }

    public double f(double x) {
        return x*x*x + a*x*x + b*x + c;
    }

    private void findIntervals() {
        double devDi = countDi(3, 2 * a, b);

        // Строго возрастает и имеет только один корень
        if (devDi <= e * e) {
            double f0 = f(0);

            if (Math.abs(f0) < e) {
                roots.add(0.0);
            }
            else if (f0 < -e) {
                global.add(new Interval(0, Double.MAX_VALUE));
            }
            else if (f0 > e) {
                global.add(new Interval(Double.MIN_VALUE, 0));
            }
        }
        // Может быть несколько нулей
        else  {
            if (beta < alpha) {
                double tmp = alpha;
                alpha = beta;
                beta = tmp;
            }

            double fa = f(alpha);
            double fb = f(beta);

            if (fa > e && fb > e) {
                global.add(new Interval(Double.MIN_VALUE, alpha));
            }
            else if (fa < -e && fb < -e) {
                global.add(new Interval(beta, Double.MAX_VALUE));
            }
            else if (fa > e && Math.abs(fb) < e) {
                roots.add(beta);
                global.add(new Interval(Double.MIN_VALUE, alpha));
            }
            else if (Math.abs(fa) < e && fb < -e) {
                roots.add(alpha);
                global.add(new Interval(beta, Double.MAX_VALUE));
            }
            else if (fa > e && fb < -e) {
                global.add(new Interval(Double.MIN_VALUE, alpha));
                local.add(new Interval(alpha, beta));
                global.add(new Interval(beta, Double.MAX_VALUE));
            }
            else if (Math.abs(fa) < e && Math.abs(fb) < e) {
                roots.add((alpha + beta) / 2);
            }
        }
    }

    private void localizeIntervals() {
        for (Interval inter: global) {
            int ind = 1;
            double d = delta;

            if (inter.right == Double.MAX_VALUE) {
                while (f(inter.left + d) < 0) {
                    d = delta * ++ind;
                }

                local.add(new Interval(inter.left + d - delta, inter.left + d));
            }
            else if (inter.left == Double.MIN_VALUE){
                while (f(inter.right - d) > 0) {
                    d = delta * ++ind;
                }

                local.add(new Interval(inter.right - d, inter.right - d + delta));
            }
        }
    }

    private void findRoots() {
        for (Interval inter : local) {
            double c = (inter.left + inter.right) / 2;

            // Метод дихотомии
            if ( (f(inter.left) > 0 && f(inter.right) < 0) || (f(inter.left) < 0 && f(inter.right) > 0) ) {
                while (Math.abs(f(c)) > e) {
                    c = (inter.left + inter.right) / 2;

                    if (f(c) > e) {
                        if (f(inter.left) > 0)
                            inter.left = c;
                        else
                            inter.right = c;
                    } else if (f(c) < -e) {
                        if (f(inter.left) < 0)
                            inter.left = c;
                        else
                            inter.right = c;
                    }
                }
            }
            // Метод хорд
            else {
                double xNext = 0;
                double tmp;
                double xPrev = inter.left;
                double xCurr = inter.right;

                do {
                    tmp = xNext;
                    xNext = xCurr - f(xCurr) * (xPrev - xCurr) / (f(xPrev) - f(xCurr));
                    xPrev = xCurr;
                    xCurr = tmp;
                } while (Math.abs(xNext - xCurr) > e);

                c = xNext;
            }

            roots.add(c);
        }
    }

    private double countDi(double a, double b, double c) {
        return b*b - 4*a*c;
    }

    private double getRoot(double a, double b, double c, int sign) {
        return (-b + sign * Math.sqrt( countDi(a, b, c) )) / (2 * a);
    }

}
